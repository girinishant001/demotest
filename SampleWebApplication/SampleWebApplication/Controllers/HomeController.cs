﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SampleWebApplication.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page, with minor change.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Small change to test by Ng, Test1.";

            return View();
        }
    }
}